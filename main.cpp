#include <iostream>
#include "NoGraphicsEngine.h"
#include "game.h"

/**
 * In order to run graphical engine, uncomment first two lines of code.
 * Comment everything else.
 * */
int main() {
    Game g;
    g.run();
      //This part will simulate rocket in a space of size (800,800). When world bounds are included, rocket can move
////      // between (10-765) in x and y coordinates.
//      std::vector<double> startingCoordinates {390,765};
//      Rocket rocket;
//      rocket.setPosition(startingCoordinates);
//      Obstacles obstacles;
//
//      /*Adding a goal here*/
//      obstacles.setGoalCoordinateX(390.0);
//      obstacles.setGoalCoordinateY(25.0);
//      obstacles.setGoalWidth(30);
//      obstacles.setGoalHeight(30);
//
//      //adding rocks here.First pair is location, second pair are width and height
//      obstacles.addARock(std::make_pair(std::make_pair(420,650),std::make_pair(25,25)));
//      obstacles.addARock(std::make_pair(std::make_pair(330,200),std::make_pair(150,60)));
//      obstacles.addARock(std::make_pair(std::make_pair(590,470),std::make_pair(60,60)));
//      obstacles.addARock(std::make_pair(std::make_pair(665,430),std::make_pair(70,30)));
//      obstacles.addARock(std::make_pair(std::make_pair(140,400),std::make_pair(125,125)));
//
//      std::ifstream inputFile("input.txt");
//      std::string line;
//      double distance=0.0,fitness=0.0;
//      while(std::getline(inputFile,line)) {
//            auto start = std::chrono::system_clock::now();
//            auto end = std::chrono::system_clock::now();
//            while(std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count() < 10) {
//                  end = std::chrono::system_clock::now();
//            }
//            std::istringstream iss(line);
//            std::string command;
//            iss >> command;
//
//            rocket.commandMove(command);
//            rocket.spendFuel();
//            rocket.addAcceleration(0,0.0981);
//            rocket.moveRocket();
//            distance =  rocket.calculateDistanceFromGoal(obstacles.getGoalCoordinateX(),obstacles.getGoalCoordinateY(),obstacles.getGoalWidth(),obstacles.getGoalHeight());
//            for(auto const &obstacle : obstacles.getRocks()) {
//                  if(rocket.checkCollision(obstacle.first.first,obstacle.first.second,obstacle.second.first,obstacle.second.second)) {
//                        rocket.setCrashed(true);
//                        fitness = rocket.fitnessFunction(distance);
//                        break;
//                  }
//            }
//            if(rocket.getCrashed())
//                  break;
//            //uspijeh
//            if(rocket.checkCollision(obstacles.getGoalCoordinateX(),obstacles.getGoalCoordinateY(),obstacles.getGoalWidth(),obstacles.getGoalHeight()) || rocket.getFuel() <= 0) {
//                  fitness = rocket.fitnessFunction(distance);
//                  break;
//            }
//      }
//      std::cout << "Distance: " << distance << std::endl;
//      std::cout << "Fitness: " << fitness << std::endl;


    return 0;
}