 /*
 * Represents a rocket class.
 * Stores values of rocket, has functions to
 *
 * */
#ifndef PROJECT_ROCKET_H
#define PROJECT_ROCKET_H
#include <vector>
#include <cmath>
#include<string>
#define THRESHOLD 0.01
class Rocket {
public:
    Rocket() : m_fuel(300), maxFuel(300),m_speedDecay(0.98),m_force(1.0),m_bound(765.0), position(2), velocity(2), acceleration(1), dimension(25.0) {};
    Rocket(double bound, double force,double speedDecay,int fuel, double dimension) :
                        m_speedDecay(speedDecay),m_force(force), m_bound(bound), m_fuel(fuel),maxFuel(fuel),position(2),velocity(2),acceleration(2),dimension(dimension) {};
    Rocket(double bound, double force,double speedDecay, std::vector<double> &position, std::vector<double> &velocity, std::vector<double> &acceleration, int fuel,double dimension)
           : m_force(force), m_bound(bound), m_speedDecay(speedDecay),m_fuel(fuel),maxFuel(fuel),position(position), velocity(velocity), acceleration(acceleration), dimension(dimension){};
    double getXPosition();
    double getYPosition();
    std::vector<double> &getPosition();
    void setPosition(std::vector<double> &position);
    std::vector<double> &getVelocity();
    void setVelocity(std::vector<double> &velocity);
    std::vector<double> &getAcceleration();
    void addAcceleration(double x, double y);
    void setAcceleration(std::vector<double> &acceleration);
    void setForce(double force);
    int getFuel();
    void setFuel(int fuel);
    int getMaxFuel();
    double getDimension();
    void spendFuel();
    double getForce();
    void moveRocket();
    bool checkCollision(double upperLeftCoordinateX, double upperLeftCoordinateY, double xLength, double yLength);
    double calculateDistanceFromGoal(double goalXCoordinate, double goalYCoordinate,double goalWidth, double goalHeight);
    double fitnessFunction(double distanceFromGoal);
    void commandMove(std::string command);
    void setCrashed(bool crashed);
    bool getCrashed();

private:
     double m_bound;
     double m_force;
     double m_speedDecay;
     double dimension;
     int m_fuel;
     const int maxFuel;
     bool hasCrashed;
     std::string commandCache;
     std::vector<double> position;
     std::vector<double> velocity;
     std::vector<double> acceleration;

     void checkBounds();
};
#endif //PROJECT_ROCKET_H
