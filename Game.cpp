//
// Created by imint on 1/15/19.
//
#include <fstream>
#include "game.h"


void Game::processEvents(Rocket &rocket) {
    sf::Event event{};
    while(renderWindow.pollEvent(event)) {
        if(sf::Event::Closed == event.type) {
            renderWindow.close();
        }
        else if(event.type == sf::Event::KeyPressed){
            double x=0,y=0;
            if((sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Keyboard::isKeyPressed(sf::Keyboard::Up) ) && rocket.getFuel() > 0) {
                y-=rocket.getForce();
                rocket.spendFuel();
                if((sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) && rocket.getFuel() > 0) {
                    rocket.spendFuel();
                    x-=rocket.getForce();
                }
                else if((sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) && rocket.getFuel() > 0) {
                    rocket.spendFuel();
                    x+=rocket.getForce();
                }
            }
            else if((sf::Keyboard::isKeyPressed(sf::Keyboard::S) || sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) && rocket.getFuel() > 0) {
                y+=rocket.getForce();
                rocket.spendFuel();
                if((sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) && rocket.getFuel() > 0) {
                    rocket.spendFuel();
                    x-=rocket.getForce();
                }
                else if((sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) && rocket.getFuel() > 0) {
                    rocket.spendFuel();
                    x+=rocket.getForce();
                }
            }
            else if((sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left) ) && rocket.getFuel() > 0) {
                x-=rocket.getForce();
                rocket.spendFuel();
                if((sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) && rocket.getFuel() > 0) {
                    rocket.spendFuel();
                    y-=rocket.getForce();
                }
                else if((sf::Keyboard::isKeyPressed(sf::Keyboard::S) || sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) && rocket.getFuel() > 0) {
                    rocket.spendFuel();
                    y+=rocket.getForce();
                }
            }
            else if((sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) && rocket.getFuel() > 0) {
                x+=rocket.getForce();
                rocket.spendFuel();
                if((sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) && rocket.getFuel() > 0) {
                    rocket.spendFuel();
                    y-=rocket.getForce();
                }
                else if((sf::Keyboard::isKeyPressed(sf::Keyboard::S) || sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) && rocket.getFuel() > 0) {
                    rocket.spendFuel();
                    y+=rocket.getForce();
                }
            }
            rocket.addAcceleration(x,y);

        }
    }

}

void Game::extractNumbers() {
    numberExtractor.clear();
    numberExtractor.str("");
    numberExtractor << "Fuel left: " << rocket.getFuel() << " " << "/" << rocket.getMaxFuel();
    fuelText.setString(numberExtractor.str());

}

void Game::renderTargets() {
    rocket.moveRocket();
    std::vector<double> pos = rocket.getPosition();
    sf::Vector2f drawn(static_cast<float> (pos[0]), static_cast<float>(pos[1]));
    drawnRocket.setPosition(drawn);
    fuelBar.setSize(sf::Vector2f(rocket.getFuel(),20.f));
    extractNumbers();
    renderWindow.draw(fuelBar);
    renderWindow.draw(fuelBarOutline);
    renderWindow.draw(drawnRocket);
    renderWindow.draw(fuelText);
    renderWindow.draw(world);
}

void Game::run() {
    renderWindow.setFramerateLimit(30);
    drawnRocket.setPosition(390.f,765.f);
    std::vector<double> d {390.0,765.};
    rocket.setPosition(d);
    internalClock.restart();
    font.loadFromFile("../LiberationSans-Regular.ttf");
    fuelText.setFont(font);
    std::ifstream inputFile("output.txt");
    std::string line;
    std::vector<std::string> vex;
    while(std::getline(inputFile,line)) {

        std::istringstream iss(line);
        std::string command;
        iss >> command;
        vex.push_back(command);
    }

    int size = vex.size();
    int index = 0;

    while(renderWindow.isOpen() && size>=0) {
        processEvents(Game::rocket);
        rocket.commandMove(vex[index++]);
        size--;

        rocket.spendFuel();
        sf::Time time = internalClock.restart();
        rocket.addAcceleration(0,0.0981);
        std::vector<sf::RectangleShape> obstacles = world.getRocks();
        for(auto const &obstacle : obstacles) {
            double x = obstacle.getPosition().x;
            double y = obstacle.getPosition().y;

            if(rocket.checkCollision(x,y,obstacle.getSize().x,obstacle.getSize().y)) {
                std::cout << "Rocket has crashed\n";
            }
        }
        sf::RectangleShape *goal = world.getGoal();
        double goalXCoordinate = goal->getPosition().x;
        double goalYCoordinate = goal->getPosition().y;

        if(rocket.checkCollision(goalXCoordinate, goalYCoordinate, goal->getSize().x, goal->getSize().y)) {
            std::cout << "Goal reached!\n";
            rocket.setFuel(0);
            std::cout << "Time = " << internalClock.getElapsedTime().asSeconds() << "\n";
            return;
        }
        renderWindow.clear();
        renderTargets();
        renderWindow.display();
    }
}