//
// Created by imint on 1/15/19.
//

#ifndef PROJECT_WORLD_H
#define PROJECT_WORLD_H
#include <SFML/Graphics.hpp>

class World : public sf::Drawable {
public:
    World();
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
    std::vector<sf::RectangleShape> &getRocks();
    void addRock(sf::RectangleShape &rock);
    sf::RectangleShape *getGoal();
private:
    sf::RectangleShape worldBounds[4];
    std::vector<sf::RectangleShape> rocks;
    sf::RectangleShape goal;
};
#endif //PROJECT_WORLD_H
