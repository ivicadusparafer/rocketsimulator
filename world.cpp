//
// Created by imint on 1/15/19.
//
#include "world.h"

World::World() {
    worldBounds[0].setPosition(sf::Vector2f(0.f,0.f));
    worldBounds[0].setSize(sf::Vector2f(800.f,10.f));
    worldBounds[1].setPosition(sf::Vector2f(0.f,790.f));
    worldBounds[1].setSize(sf::Vector2f(800.f,10.f));
    worldBounds[2].setPosition(sf::Vector2f(0.f,0.f));
    worldBounds[2].setSize(sf::Vector2f(10.f,800.f));
    worldBounds[3].setPosition(sf::Vector2f(790.f,0.f));
    worldBounds[3].setSize(sf::Vector2f(10.f,800.f));

    sf::RectangleShape rock0,rock1,rock2,rock3,rock4;
    rock0.setFillColor(sf::Color::Red);
    rock1.setFillColor(sf::Color::Red);
    rock2.setFillColor(sf::Color::Red);
    rock3.setFillColor(sf::Color::Red);
    rock4.setFillColor(sf::Color::Red);

    //Rocks are hardcoded for this
    rock0.setPosition(420,650);
    rock0.setSize(sf::Vector2f(45.f,45.f));
    addRock(rock0);

    rock1.setPosition(330,200);
    rock1.setSize(sf::Vector2f(150.f,50.f));
    addRock(rock1);

    rock2.setPosition(590,460);
    rock2.setSize(sf::Vector2f(60.f,60.f));
    addRock(rock2);

    rock3.setPosition(665,330);
    rock3.setSize(sf::Vector2f(70,30));
    addRock(rock3);

    rock4.setPosition(140,400);
    rock4.setSize(sf::Vector2f(125,125));
    addRock(rock4);



    goal.setPosition(sf::Vector2f(390.f, 25.f));
    goal.setSize(sf::Vector2f(30.f,30.f));
    goal.setFillColor(sf::Color::Green);

    for (auto &worldBound : worldBounds) {
        worldBound.setFillColor(sf::Color::Cyan);
    }
}

void World::draw(sf::RenderTarget &target, sf::RenderStates states) const {
    for (const auto &worldBound : worldBounds) {
        target.draw(worldBound);
    }

    for(const auto &rock : rocks) {
        target.draw(rock);
    }

    target.draw(goal);
}


sf::RectangleShape* World::getGoal() {
    return &goal;
}

std::vector<sf::RectangleShape>& World::getRocks() {
    return rocks;
}

void World::addRock(sf::RectangleShape &rock) {
    rocks.push_back(rock);
}
