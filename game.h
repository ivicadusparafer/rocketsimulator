//
// Created by imint on 1/15/19.
//

#ifndef PROJECT_GAME_H
#define PROJECT_GAME_H
#include "rocket.h"
#include "world.h"
#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>
#include <sstream>

class Game{
public:
    Game() : renderWindow(sf::VideoMode(800,800),"Rocket game!"){
        drawnRocket.setSize(sf::Vector2f(static_cast<float>(rocket.getDimension()), static_cast<float>(rocket.getDimension())));
        drawnRocket.setFillColor(sf::Color::Green);
        fuelBar.setPosition(10,10);
        fuelBar.setFillColor(sf::Color::Blue);
        fuelBarOutline.setPosition(10,10);
        fuelBarOutline.setOutlineColor(sf::Color::White);
        fuelBarOutline.setFillColor(sf::Color::Transparent);
        fuelBarOutline.setOutlineThickness(2.75f);
        fuelBarOutline.setSize(sf::Vector2f(rocket.getMaxFuel(),20.f));
        fuelText.setPosition(15,15);
        fuelText.setCharacterSize(14);
        fuelText.setColor(sf::Color::White);
    };
    void processEvents(Rocket &rocket);
    void renderTargets();
    void extractNumbers();
    void run();
private:
    Rocket rocket;
    World world;
    sf::RenderWindow renderWindow;
    sf::Clock internalClock;
    sf::RectangleShape drawnRocket;
    sf::RectangleShape fuelBar;
    sf::RectangleShape fuelBarOutline;
    sf::Text fuelText;
    sf::Font font;
    std::stringstream numberExtractor;


};
#endif //PROJECT_GAME_H
