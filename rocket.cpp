//
// Created by imint on 1/15/19.
//
#include <iostream>
#include "rocket.h"

double Rocket::getXPosition() {
    return position[0];
}

double Rocket::getYPosition() {
    return position[1];
}

std::vector<double>& Rocket::getPosition() {
    return position;
}

void Rocket::setPosition(std::vector<double> &position) {
    Rocket::position = position;
}

std::vector<double>& Rocket::getVelocity() {
    return velocity;
}

void Rocket::setVelocity(std::vector<double> &velocity) {
    Rocket::velocity = velocity;
}

std::vector<double>& Rocket::getAcceleration() {
    return acceleration;
}

void Rocket::setAcceleration(std::vector<double> &acceleration) {
    Rocket::acceleration = acceleration;
}

void Rocket::addAcceleration(double x, double y) {
    Rocket::acceleration[0] += x;
    Rocket::acceleration[1] += y;
}

double Rocket::getForce() {
    return Rocket::m_force;
}

void Rocket::setForce(double force) {
    Rocket::m_force = force;
}

int Rocket::getFuel() {
    return m_fuel;
}

int Rocket::getMaxFuel() {
    return maxFuel;
}

double Rocket::getDimension() {
    return dimension;
}
void Rocket::setFuel(int fuel) {
    m_fuel = fuel;
}

void Rocket::spendFuel() {
    if(m_fuel > 0)
        m_fuel--;
}
void Rocket::checkBounds() {
    if(position[0] < 10.0) {
        //adding decay to speed here
        if(velocity[0] < 0.0) {
            velocity[0] *= -0.75;
        }
        if(std::abs(velocity[0]) <= THRESHOLD) {
            velocity[0] = 0;
        }
        position[0] = 10.0;
    }
    else if(position[0] > m_bound) {
        if(velocity[0] > 0.0) {
            velocity[0] *= -0.75;
        }
        if(std::abs(velocity[0]) <= THRESHOLD) {
            velocity[0] = 0;
        }
        position[0] = m_bound;
    }
    if(position[1] < 10.0) {
        if(velocity[1] < 0.0) {
            velocity[1] *= -0.75;
        }
        if(std::abs(velocity[1]) <= THRESHOLD) {
            velocity[1] = 0;
        }
        position[1] = 10.0;
    }
    else if(position[1] > m_bound) {
        if(velocity[1] > 0.0) {
            velocity[1] *= -0.75;
        }
        if(std::abs(velocity[1]) <= THRESHOLD) {
            velocity[1] = 0;
        }
        position[1] = m_bound;
    }
}


//Force (representing thrust) is added as acceleration. Depending on pressed key, this will be stored in acceleration.
//When a key is pressed force added is defined inside rocket.h as member m_force.
void Rocket::moveRocket() {

    velocity[0] += acceleration[0];
    velocity[1] += acceleration[1];
    //velocity decay with fixed friction factor and rounding
    velocity[0] *= m_speedDecay;
    velocity[1] *= m_speedDecay;
    position[0] += velocity[0];
    position[1] += velocity[1];
    checkBounds();
    acceleration[0] = 0.0;
    acceleration[1] = 0.0;
}

bool Rocket::checkCollision(double upperLeftCoordinateX, double upperLeftCoordinateY, double xLength, double yLength) {
    double rocketCoordinateX{position[0]}, rocketCoordinateY{position[1]};
    double bottomRockBorder{upperLeftCoordinateY + yLength}, bottomRocketBorder{rocketCoordinateY + dimension};
    double rightRockBorder{upperLeftCoordinateX + xLength}, rightRocketBorder{rocketCoordinateX + dimension};

    if ((rocketCoordinateX >= upperLeftCoordinateX && rightRocketBorder <= rightRockBorder) ||
            (rocketCoordinateX <= rightRockBorder && rightRocketBorder >= rightRockBorder) ||
            (rocketCoordinateX <= upperLeftCoordinateX && rightRocketBorder >= upperLeftCoordinateX)) {

        if((rocketCoordinateY >= upperLeftCoordinateY && bottomRocketBorder <= bottomRockBorder) ||
                (bottomRocketBorder >= bottomRockBorder && rocketCoordinateY <= bottomRockBorder) ||
                (bottomRocketBorder >= upperLeftCoordinateY && rocketCoordinateY <= upperLeftCoordinateY)) {
            std::cout << "------ COLLISION OCCURRED ------\n";
            hasCrashed = true;
            return true;
        }
    }
    hasCrashed = false;
    return false;
}

double Rocket::calculateDistanceFromGoal(double goalXCoordinate, double goalYCoordinate, double goalWidth,double goalHeight) {
    double goalX = goalXCoordinate + goalWidth/2;
    double goalY = goalYCoordinate + goalHeight/2;
    double rocketX = position[0] + dimension/2;
    double rocketY = position[1] + dimension/2;
    double x,y;
    x = (goalX - rocketX) * (goalX - rocketX);
    y = (goalY - rocketY) * (goalY - rocketY);
    return sqrt(x+y);
}

double Rocket::fitnessFunction(double distanceFromGoal) {
    double punishment = hasCrashed ? 1400 : 0;
    return punishment + distanceFromGoal + (maxFuel-m_fuel);
}

void Rocket::commandMove(std::string command) {
    if(command == "1") {
        addAcceleration(0,-getForce());
    }
    else if(command == "3") {
        addAcceleration(-getForce(),0);
    }
    else if(command == "2") {
        addAcceleration(0,+getForce());
    }
    else if(command == "4") {
        addAcceleration(+getForce(),0);
    }
}

void Rocket::setCrashed(bool crashed) {
    hasCrashed = crashed;
}

bool Rocket::getCrashed() {
    return hasCrashed;
}