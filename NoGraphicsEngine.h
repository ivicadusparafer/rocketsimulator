//
// Created by imint on 1/21/19.
//

#ifndef PROJECT_NOGRAPHICSENGINE_H
#define PROJECT_NOGRAPHICSENGINE_H
#include "rocket.h"
#include <chrono>
#include <sstream>
#include <fstream>
/**
 * Chrono header is included to measure time in learning.
 * Input will come every 10 milliseconds.
 * */
class Obstacles{
public:

    double getGoalCoordinateX();
    double getGoalCoordinateY();
    double getGoalWidth();
    double getGoalHeight();
    void setGoalCoordinateX(double coordinateX);
    void setGoalCoordinateY(double coordinateY);
    void setGoalWidth(double goalWidth);
    void setGoalHeight(double goalHeight);
    void addARock(std::pair<std::pair<double, double>, std::pair<double,double> >rock);
    std::vector<std::pair<std::pair<double,double>, std::pair<double,double> > >&getRocks();
private:
    double goalCoordinateX;
    double goalCoordinateY;
    double goalWidth;
    double goalHeight;
    std::vector<std::pair<std::pair<double,double> , std::pair<double,double> > > rocks;
};
#endif //PROJECT_NOGRAPHICSENGINE_H
