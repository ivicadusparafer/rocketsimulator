//
// Created by imint on 1/21/19.
//
#include "NoGraphicsEngine.h"

double Obstacles::getGoalCoordinateX() {
    return goalCoordinateX;
}

double Obstacles::getGoalCoordinateY() {
    return goalCoordinateY;
}

double Obstacles::getGoalWidth() {
    return goalWidth;
}

double Obstacles::getGoalHeight() {
    return goalHeight;
}

void Obstacles::setGoalCoordinateX(double coordinateX) {
    goalCoordinateX = coordinateX;
}

void Obstacles::setGoalCoordinateY(double coordinateY) {
    goalCoordinateY = coordinateY;
}

void Obstacles::setGoalHeight(double goalHeight) {
    Obstacles::goalHeight = goalHeight;
}

void Obstacles::setGoalWidth(double goalWidth) {
    Obstacles::goalWidth = goalWidth;
}

void Obstacles::addARock(std::pair<std::pair<double, double>, std::pair<double,double> > rock) {
    rocks.push_back(rock);
}

std::vector<std::pair<std::pair<double,double>, std::pair<double,double> > >& Obstacles::getRocks() {
    return rocks;
}